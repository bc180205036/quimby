import React from 'react'
import { BiRun } from 'react-icons/bi'
import Footer from './Footer'
function FrontPageBlock() {
  return (
<>
<div className='container mt-3'>
    <div className='row ms-3' >
        <div className='col-3'><div className='ms-3'><BiRun className='fs-2'/></div><div className='hardknot'>Hardknot</div></div>
        <div className='col d-flex justify-content-between menuitemblock'>
          <a href='/#' className='menuitems text-dark'><b>Home</b></a>
          <a href='/#' className='menuitems text-dark'>Gallary</a>
          <a href='/#' className='menuitems text-dark'>About</a>
          <a href='/#' className='menuitems text-dark'>Contact</a>
          <a href='/#' className='menuitems text-dark'>Blog</a>
          <a href='/#' className='menuitemsl text-white bg-dark'>Shop</a>
          </div>
    </div>
</div>
<div className='container ms-2 me-4 mt-4'>
  <div className='row'>
<div className='col  image1'></div>
<div className='col image2'></div>
<div className='col ms-1 image3'></div>
  </div>
  <div className='row'>
<div className='col image4'></div>
<div className='col mix-images  p-0 '> <div className='image5'></div>
<div className='image6 mt-1'></div></div>
<div className='col ms-1 image7'></div>
  </div>
</div>
<div className='container mt-1'>
  <div className='row d-flex justify-content-center'>
    
    <div className='col '>
      <span className='text ms-5'>In a world that&#39;s heating up, speeding up, and increasingly interconnected,</span>
    </div>
    
  </div>
  <div className='row d-flex justify-content-center ms-5'>
  
      <div className='text ms-5 ps-5'>there&#39;s so much that can&#39;t wait—and can be made better.</div>
    
  </div>

</div>
<div className='container  mt-5 pt-5'>
          <div className='row  '>
            <div className='col pt-4 ps-4 '>
              <div className='block-heading'>What  to wear for the holidays?</div>
              <div className='text'>You deserve a website truly represents your dreams and ideas. Quimby will design a website for you that wows your customers or readers!</div>
              <button className='btn btn-outline-dark mt-3 '>Learn More</button>
            </div>
        
            <div className='col card5-col2 ps-5 image8 me-5 mb-3'></div>
          </div>
        </div>
   <div className='container mt-5 p-5  cloud-border me-5'><div className='row'>
    <div className='col'>
      <div className='block-heading mb-4'>The Little Things</div>
    <div className='image9 '> </div>
    </div>
    <div className='col p-0'>
      <div className='image10 mb-2'></div>
    <div className='image11'></div>
    </div>
    <div className='col'>
      <div className='image12 mb-2'></div>
    <div className='image13' ></div>
    </div>
    </div></div>
    <div className='contaner mt-4 ms-5 me-5 mb-5'>
      <div className='row'>
        <div className='col image14'><div className='bdr '><button className=' bdr-btn '><b>Shop</b></button></div></div>
        <div className='col image15'><button className=' bdr-btn2 '><b>About</b></button></div>
        <div className='col image16'><button className=' bdr-btn3 '> <b>Blog</b></button></div>
      </div>
    </div>
    <div className='container mb-3'>
      <div className='row d-flex justify-content-center'>
        <div className='col-8'>
          <label className='fs-5'><b>Stay in he know!</b></label>
          <div className='input-field d-flex justify-content-between'>
            <input type='text' placeholder='Type Your Email' className='mainfield'/>
            <button className='input-btn'>Send</button>
          </div>
        </div>
      </div>
    </div>
<Footer />
</>
  )
}

export default FrontPageBlock