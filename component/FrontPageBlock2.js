import React from 'react'
import {TbSearch} from "react-icons/tb"
function FrontPageBlock2() {
  return ( 
    <div>
        <div className='container-fluide'>
          <div className='row d-flex flex-row'>
            <div className='col-10'>
        <div className='cs-chat-text-block'>
          <div className='col-9 ms-4 text-white p-5'>Hey there! My name is Quimby, it&#39;s great to meet you. I see you&#39;re in need of a website. Please share more about the purpose of your site or choose from the options below!</div>
        
      
        </div>
        <div className=' catagory-block ps-3 pt-2 pe-2 mt-2'>
        <div className='row pe-5 '>
          
        <div className='catagory-block-heading'>Suggested Website Categories</div>
          <div className='catagory-block-input d-flex justify-content-between mt-2 '>
         <input type='text' placeholder='Search a website category'  className='catagory-block-main-input'/><button className='catagory-search-button'><TbSearch className='ct-search-icon'/></button>
          </div>
          <div className='mt-4 pe-5'>
            <div className='d-flex justify-content-between'>
              <button className='catogories'>Online Store</button>
            <button className='catogories-active'>Local Business</button>
            <button className='catogories'>A Blog</button></div>

            <div className='d-flex justify-content-between mt-3 mb-3'><button className='catogories'>A Blog</button>
            <button className='catogories'>Personal Brand</button>
            <button className='catogories'>NonProfit</button></div>
            
            <button className='catogories'>School Project</button>
          </div>
          
          
        </div>
        </div>
        </div>
        <div className='col'>
          <div className='chat-circle'></div>
        </div>
        </div>
        </div>
        <div className='container mt-4 '>
          <div className='row'>
            <div className='col-10 block2 pt-4  pb-5'>
            <div className='text-white ms-3 '>Are you offering product online?</div>
            <div className='d-flex mt-3'>
              <button className='active-btn m-2' >Yes</button>
              <button className='catogories m-2' >No</button>
              </div>
          </div>
          <div className='col'>
          <div className='chat-circle'></div>
        </div>

          </div>
        </div>
        <div className='container mt-4'>
          <div className='row'>
            <div className='col-10 block2 ps-3 pt-5 pb-5'>
            <div className='text-white'>Currently, I (Quimby) do not offer an online store option.
However, I am linked with my Friends at Village Finds.
You can launch a Village Finds store and connect it with
your Quimby website.
Village Finds is a platform dedicated to small makers and
growers looking to grow their businesses together on
one marketplace.</div>
            
          </div>
          <div className='col'>
          <div className='chat-circle'></div>
        </div>

          </div>
        </div>
        <div className='container mt-4'>
          <div className='row'>
            <div className='col-10 block2 pt-3 pb-5 '>
            <div className='text-white ms-3 mb-2'>Would you like to open a Village Finds account?</div>
            <div className='d-flex '>
              <button  className='active-btn m-2'>No</button>
              <button className='catogories m-2' >Custom Options</button>
              <button className='outline-btn m-2' > Hold Off</button>
              </div>
          </div>
          <div className='col'>
          <div className='chat-circle'></div>
        </div>

          </div>
        </div>
        <div className='container mt-4'>
          <div className='row'>
            <div className='col-10 block2 ps-3 pt-5 pb-5'>
              <div className='text-white mb-3'><b>Custom it is!</b></div>
            <div className='text-white'>
In this case, I have the ability to create a fully customizable
store. This option will require a conversation with someone
on the Quimby team and will be an additional cost. If you
would like to learn more about this option, click or type Yes
below. An email will be sent to you after completing the next
steps in the Quimby process.</div>
            
          </div>
          <div className='col'>
          <div className='chat-circle'></div>
        </div>

          </div>
        </div>
        <div className='container mt-4'>
          <div className='row'>
            <div className='col-10 block2 pt-3 pb-5 '>
            <div className='text-white ms-3 mb-2'>Please confirm you are interested in a custom store option</div>
            <div className='d-flex '>
              <button  className='active-btn m-2'>No</button>
              <button className='catogories m-2' >Yes</button>
              <button className='outline-btn m-2' > Village Findes</button>
              </div>
          </div>
          <div className='col'>
          <div className='chat-circle'></div>
        </div>

          </div>
        </div>
        <div className='container mt-4 mb-4'>
          <div className='row'>
            
          <div className='col'>
          <div className='chat-dark-circle'></div>
        </div>
        <div className='col-10 block3 pt-3 ps-4 pb-3 '>
            yes
          </div>
          </div>
        </div>
        
        <div className='container-fluide'>
          <div className='row d-flex flex-row'>
            <div className='col-10'>
        <div className='cs-chat-text-block pt-5'>
          <div className=' co-11 ms-4 text-white ps-5 mb-3 '><b>Let's get back to setting up your Quimby website.</b></div>
          <div className='col-11 ms-4 text-white ps-5 mt-'>
Next, share more about what your website will offer.
You can chat me or select from the options below.</div>
        
      
        </div>
        <div className=' catagory-block ps-3 pt-2 pe-2 mt-2'>
        <div className='row pe-5 '>
          
        <div className='catagory-block-heading'>Suggested Services</div>
          <div className='catagory-block-input d-flex justify-content-between mt-2 '>
         <input type='text' placeholder='Search a website category'  className='catagory-block-main-input'/><button className='catagory-search-button'><TbSearch className='ct-search-icon'/></button>
          </div>
          <div className='mt-4 pe-2'>
            <div className='d-flex justify-content-between'>
              <button className=' catogories-active'>Jewelry</button>
            <button className=' catogories'>Beauty</button>
            <button className='catogories'>Fashion</button></div>

            <div className='d-flex justify-content-between mt-3 mb-3'>
              <button className='catogories'>Personal Care</button>
            <button className='catogories'>Consumer Goods</button>
            <button className='catogories'>Good & Beverage</button></div>
            <div className='d-flex justify-content-between mt-3 mb-3'>

            <button className='catogories'>Health & Wellness</button>
            <button className='catogories'>Sport Equipment</button>
            <button className='catogories'>Natrualpathic</button></div>
          

            
          </div>
          
          
        </div>
        </div>
        </div>
        <div className='col'>
          <div className='chat-circle'></div>
        </div>
        </div>
        </div>
        <div className='container-fluide'>
          <div className='row d-flex flex-row'>
            <div className='col-10'>
        <div className='cs-chat-text-block pt-5'>
          <div className=' co-11 ms-4 text-white ps-5 mb-3 '><b> I have a feeling this website is going to be amazing!</b></div>
          <div className='col-11 ms-4 text-white ps-5 mt-'>
         
Next, share more about your design style. I want to create
the perfect website with your vision's look and feel. You can
choose all that apply.</div>
        
      
        </div>
        <div className=' catagory-block ps-3 pt-2 pe-2 mt-2'>
        <div className='row pe-5 '>
          
        <div className='catagory-block-heading'>Suggested Services</div>
          <div className='catagory-block-input d-flex justify-content-between mt-2 '>
         <input type='text' placeholder='Search a website category'  className='catagory-block-main-input'/><button className='catagory-search-button'><TbSearch className='ct-search-icon'/></button>
          </div>
          <div className='mt-4 pe-2'>
            <div className='d-flex justify-content-between'>
              <button className=' catogories-active'>Minimalism</button>
            <button className=' catogories'>Nature</button>
            <button className='catogories'>High-End</button></div>

            <div className='d-flex justify-content-between mt-3 mb-3'>
              <button className='catogories'>
Colorful</button>
            <button className='catogories'>Image Heavy</button>
            <button className='catogories'>Industrial</button></div>
            <div className='d-flex justify-content-between mt-3 mb-3'>

            <button className='catogories'>Natural</button>
            <button className='catogories'>Text Heavy</button>
            <button className='catogories'>Business to Business</button></div>
      
          </div>
          
          
        </div>
        </div>
        </div>
        <div className='col'>
          <div className='chat-circle'></div>
        </div>
        </div>
        </div>
        <div className='container mt-4'>
          <div className='row'>
            <div className='col-10 block2 ps-3 pt-5 pb-5'>
              <div className='text-white mb-3'><b> have a feeling this website is going to be amazing!</b></div>
            <div className='text-white'>
           
Next, I'd like to use your social media to make sure your
website has all of the necessary information and images.
Just share you facebook or Instagram link in the chat!</div>
            
          </div>
          <div className='col'>
          <div className='chat-circle'></div>
        </div>

          </div>
        </div>
        <div className='container mt-4 mb-4'>
          <div className='row'>
            
          <div className='col'>
          <div className='chat-dark-circle'></div>
        </div>
        <div className='col-10 block3 pt-3 pb-3 '>
        https://www.facebook.com/FresherChoice
          </div>
          </div>
        </div>

        <div className='container mt-4'>
          <div className='row'>
            <div className='col-10 block2 ps-5 pt-5 pb-5 '>
            <div className='text-white '>
           
            Take a look and make sure I have everything correct
before we move forward.</div>
            
          </div>
          <div className='col'>
          <div className='chat-circle'></div>
        </div>

          </div>
        </div>


        <div className='container mt-4 mb-4'>
          <div className='row'>

        <div className='col block3 pt-3 pb-3 '>
       <b>Company Name:</b>  Misfit Toys, LLC
          </div>
          </div>
        </div>
        <div className='container mt-4 mb-4'>
          <div className='row'>

        <div className='col block3 pt-3 pb-3 '>
       <b> Phone Number:</b> 203-228-8813
          </div>
          </div>
        </div>
        <div className='container mt-4 mb-4'>
          <div className='row'>

        <div className='col block3 pt-3 pb-3 '>
        <b>Address:</b> 131 Capitol Avenue, Waterbury, Ct 06706
          </div>
          </div>
        </div>
        <div className='container mt-4 mb-4'>
          <div className='row'>

        <div className='col block3 pt-5  ps-2 pb-5 '>
        <b>About: </b>What if local people were empowered to organize small make
in their communities to help them connect with customers looking for
selling? Fresher Choice is making this possible through our Vendor
          </div>
          </div>
        </div>

        
        <div className='container mt-4'>
          <div className='row'>
            <div className='col-10 block2 ps-5 pt-5 pb-5 '>
            <div className='text-white '>
           
            I have what i need to create your website. If you're ready,
click the button bellow and what me make magic!.</div>
            
          </div>
          <div className='col'>
          <div className='chat-circle'></div>
        </div>
</div>
    </div>    
      
    <div className='container mt-4 mb-4'>
          <div className='row'>

        <div className='col block4 pt-3  ps- pb-3  d-flex justify-content-center'>
        <b>Generate Website</b>
          </div>
          </div>
        </div>

    </div>
  )
}

export default FrontPageBlock2