import React from 'react'
import Image from 'next/image'
import Link from 'next/link';
import { useRouter } from 'next/router';
import {TbSearch} from "react-icons/tb"
function Header() {
  const router=useRouter();
  const menue=[
    {
      name:"Home",
      path:"/",
    },
    {
      name:"Sign In",
      path:"/SignIn",

    },   {
      name:"Testimonials",
      path:"/Testimonials",

    },   {
      name:"Go Custom",
      path:"/GoCustom",

    }
  ]
   console.log(router.pathname);
  const isActive = (path) => {
    return router.pathname === path ? "active " : '';
};
  return (
    <div className='conainer-fluid header'>
      <div className='container d-flex d-flex-row justify-content-between'>
        <div><TbSearch className='logo'/><span className='fs-5'><b>Quimby</b></span></div>
        <div className=' d-flex  justify-content-between  menu_bar'> 
        {
                                   menue.map((item, index) => (
                                        <Link legacyBehavior href={item.path} key={index} passHref>
                                             <a className={`link  ${isActive(item.path)}`} >
                                                  
                                                  <div className={`link_text ${isActive(item.path)}`}>{item.name}</div>
                                             </a>
                                        </Link>
                                        

                                   ))
                              }
        </div>
        
        </div>

    </div>
  )
}

export default Header