import React from 'react'
import styles from './Input.module.css';
const Inputfield = () => {
    return (
        <div className='container'>
            <div>
                <div className={` ${styles.card1_ol1_input} d-flex justify-content-between `}>
                    <input type='text' placeholder='Enter your email' className={`${styles.input_field}`} /><button className={`${styles.button}`}><span className={`${styles.text}`}>Discover Your AI Website</span></button></div>
            </div>
        </div>
    )
}

export default Inputfield