import React from 'react'
import Link from 'next/link'
 
 import {TiSocialFacebook} from 'react-icons/ti'
 import {SlSocialInstagram,SlSocialYoutube} from 'react-icons/sl'
function Footer() {
  return (
    <div className='container-fluid bg-dark footer'>
      <div className='container '>
        <div className='row '>
          <div className='col pt-5 pb-5 '>
            <div><span className='text-white fs-5'><b>Navegate</b></span></div>
            <div className='mt-4'>
            <div><a className='footer-link' href='/'>Home</a></div>
            <div><a className='footer-link' href='/SignIn'>Sign In</a></div>
            <div><a className='footer-link' href='Testimonials'>Testimonials</a></div>
           
           <div><a className='footer-link' href='/GoCustom'>Go Custom</a></div>
           </div>
          </div>
          <div className='col pt-5 pb-5'>
          <div><span className='text-white fs-5'><b>Partners</b></span></div>
            <div className='mt-4'>
            <div><span className='text-white'>Vilage Finds</span></div>
            
           </div>
          </div>
          <div className='col pt-5 pb-5'></div>
          <div className='col pt-5 pb-5'></div>
          <div className='col pt-5 pb-5'></div>
          <div className='col pt-5 pb-5'></div>
          <div className='col pt-5 pb-5'>
          <div><span className='text-white fs-5'><b>Contact</b></span></div>
            <div className='mt-4'>
            <div><span className='text-white'>(401) 594-9428</span></div>
            <div className='mt-1'><span  className='text-white'>brandon@fresherchoice.com</span></div>
            <div className='d-flex mt-2 '>
              <a href='/' className='social-icons '><TiSocialFacebook className='social-icons-text fs-3'/></a>
              <a href='/' className='social-icons '><SlSocialInstagram className='social-icons-text fs-5'/></a>
              <a href='/' className='social-icons '><SlSocialYoutube className='social-icons-text fs-5'/></a>
            </div>
            
           </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer