import Head from 'next/head'
import Image  from 'next/image';
import Header from '@/component/header'
import styles from '@/styles/Home.module.css'
import Footer from '@/component/Footer'
import { BiPlay } from 'react-icons/bi'
import { FaCircle } from 'react-icons/fa'
import {BsTriangleFill} from 'react-icons/bs'
import Inputfield from '@/component/inputfield';
import FrontPageBlock from '@/component/FrontPageBlock';
import FrontPageBlock2 from '@/component/FrontPageBlock2'
function Home(){
return (
    <>
      <Head>
        <title>Quimby</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

      </Head>
      <main >
        {/* Starting Header of page */}
        <Header />
        {/* Starting Card 1 of page */}
        <div className='container-fluide card1'>
          <div className='row'>
            <div className='col '>
              <div className='card1-col1-heading'><h1 className='text-white card1-col1-heading-text'>Let Quimby AI make your website</h1></div>
              <div className='card1-col1-input d-flex justify-content-between '><input type='text' placeholder='Enter your email' className='ps-4 card1-col1-input-feild' /><button className='card1-col1-button'><span className='card1-col1-button-text'>Discover Your AI Website</span></button></div>
              <div className='card1-col1-peragraf'><p className='text-white'>It &#39;s hard and expensive building websites from scratch and let&#39;s face it, most of the drag and drop website builders all look the same. This is why we&#39;ve launched Quimby. One click and you can generate a beautifully designed website in seconds.</p></div>
            </div>
            <div className='col-5 bg-white m-5 card1-col2  pe-3 ps-'>< FrontPageBlock/></div>
          </div>
        </div>
        {/* Heading h2*/}
      
        {/* card 2 */}
        <div className='container card2 mt-5 mb-4 '>
          <div className='row d-flex justify-content-center'>
            <div className='col-6 card2-col1 p-0'> <FrontPageBlock2/></div>
            <div className='col-6 card2col2 ps-5 pt-5 pb-5'>
              <div><h2 className='heading2   '>Chat with Quimby</h2></div>
              <div><p>Let Quimby know what kind of website you need and Quimby <br />
                will create a website that brings your unique vision to life.</p>
              </div>
              <div>
                <ul className='card2-col2-ul'>
                  <li><BiPlay /><span className='ms-1'>I&#39;ts Fast It&#39;s  </span></li>
                  <li><BiPlay /><span className='ms-1'>Designed Well</span></li>
                  <li><BiPlay /><span className='ms-1'>It&#39;s Simple</span></li>
                </ul>
              </div></div>
          </div>
        </div>
        {/* card3 */}
        <div className='container card3 mt-5'>
          <div className='row'>
            <div className='col-4'><div className='grid'><div className='row'><div className='d-flex'>
              <div className='cricle-yellow  m-1'><FaCircle /></div>
              <div className='text-danger m-1'><FaCircle /></div>
              <div className='text-success m-1'><FaCircle /></div>
            </div>
              <div className='row mt-3  '>
                <div className='col-7'>
                  <div className='line-bar1'></div>
                  <div className='line-bar2'></div>
                  <div className='line-bar3'></div>
                </div>
                <div className='col-5'>

                  <div className='row d-flex '>
                    <div className='col-7 line-bar4 me-1'> </div>
                    <div className='col-3 line-bar5'> </div>

                  </div>
                  <div className='row line-bar6 mt-1 '></div>
                </div>
              </div></div></div><span>Let Quimby know what kind of website you need and Quimby will create a website that brings your unique vision to life.</span></div>
            <div className='col-4 '><div className='grid pt-3'>
              <div className='row ps-4 pt-4 '>
                <div className='col abc-box'><span className='abc'>Abc</span></div>
                <div className='col'><div className=' triangle-box'><BsTriangleFill className='triangle1'/><FaCircle className='circle1'/></div></div>
              </div>
              <div className='row mt-4 '>
                <div className='d-flex flex-row-reverse'>  <div className='cricle-yellow text-danger  m-1'><FaCircle /></div>
              <div className='text-warning m-1'><FaCircle/></div>
              <div className='text-success m-1'><FaCircle/></div>
              <div className='text-dark m-1'><FaCircle/></div>
              <div className=' text-secondary m-1'><FaCircle/></div>
              <div className='text-info m-1'><FaCircle/></div>
              
              
              </div>
            
              </div>
            </div><span>Let Quimby know what kind of website you need and Quimby will create a website that brings your unique vision to life.</span></div>
            <div className='col-4'><div className='grid pt-3 ps-3'>
              <div className=''><FaCircle className='circle2'/></div>
              <div className='line-bar7 mt-1'></div>
              <div className='line-bar8 mt-1'></div>
              <div className='triangle-box2 mt-2'> <div className='inner-box'><BsTriangleFill className='triangle2'/><FaCircle className='circle3'/></div></div>
              </div><span>Let Quimby know what kind of website you need and Quimby will create a website that brings your unique vision to life.</span></div>
          </div>
        </div>
        <div className='container d-flex justify-content-center mt-5'><span className='heading2'>Industries</span></div>
        <div className='container card4 d-flex justify-content-between p-5'>
          <div className='industries'>The Arts</div>
          <div className='industries' >Small Business</div>
          <div className='industries'>Non Profit</div>
          <div className='industries'>Houses of Worship</div>
          <div className='industries'>Portfolios</div>
          <div className='industries'>Portfolios</div>
        <div>  <div className='more '>More+</div></div>
          

        </div>
        <div className='container-fluide card5 mt-5 pt-5'>
          <div className='row  '>
            <div className='col card5-col1  '>
              <div className='heading2'>Beautiful Design</div>
              <div className='text'>You deserve a website truly represents your dreams and ideas. Quimby will design a website for you that wows your customers or readers!</div>
            </div>
        
            <div className='col-7 card5-col2 ps-5'>     <Image
      src="/2.jpg"
      width={900}
      height={400}
      className='card5-image1'
      alt="Picture of the author"
    /> </div>
          </div>
        </div>
        <div className='container-fluide card6 mt-5 pt-5'>
          <div className='row  '>
            
        
            <div className='col-7 card6-col1 '>     <Image
      src="/3.jpg"
      width={900}
      height={400}
      className='card6-image1'
      alt="Picture of the author"
    /> </div>
    <div className='col card6-col2  '>
              <div className='heading2'>Beautiful Design</div>
              <div className='text'>You deserve a website truly represents your dreams and ideas.Quimby will design a website for you that wows your customers or readers!</div>
            </div>
          </div>
        </div>
        
        <div className='container mt-5'>
          <div className='row d-flex justify-content-center'>
            <div className='col-8'>
              <div><h2 className='heading1'>Our Customer Love Quimby</h2></div>
            </div>
          </div>
        </div>
        <div className='container-fluide ps-5 pe-5 mt-5'>
          <div className='row ps-5 pe-5'> 
          <div className='col-4 ps-5 pe-5'>
            <div className='row'>
              <div className='col-3  picture-circle'><Image src="/4.jpg" width={40} height={40} alt='my'/></div>
              <div className='col'><div><h5 className='heading5'>Jerry Quan</h5></div>
              <div><p>Jerry&#39;s Ice Cream Shop</p></div></div>
            </div>
            <div className='row'>Let Quimby know what kind of website you need and Quimby will create a website that brings your unique vision to life.</div>
            
            </div>
            <div className='col-4 ps-5 pe-5'>
            <div className='row'>
              <div className='col-3 picture-circle'><Image src="/4.jpg" width={40} height={40} alt='my'/></div>
              <div className='col'><div><h5 className='heading5'>Jerry Quan</h5></div>
              <div><p>Jerry&#39;s Ice Cream Shop</p></div></div>
            </div>
            <div className='row'>Let Quimby know what kind of website you need and Quimby will create a website that brings your unique vision to life.</div>
            
            </div>
            <div className='col-4 ps-5 pe-5'>
            <div className='row'>
              <div className='col-3 picture-circle'><Image src="/4.jpg" width={40} height={40} alt='my'/></div>
              <div className='col'><div><h5 className='heading5'>Jerry Quan</h5></div>
              <div><p>Jerry&#39;s Ice Cream Shop</p></div></div>
            </div>
            <div className='row'>Let Quimby know what kind of website you need and Quimby will create a website that brings your unique vision to life.</div>
            
            </div>
            </div>
        </div>
        <div className='container mt-5 mb-5'>
          <div className='row d-flex justify-content-center'>
            <div className='col-5'>
              <div className='ms-4'><span className='heading0 ps-5'>Let Quimby AI</span></div>
              <div className='ms-4'><span className='heading0'> make your website</span></div>
              
              <div><Inputfield/></div>
              <div className='ms-4'>It&#39;s hard and expensive building websites from scratch and let&#39;s face it, most of the drag and drop website builders all look the same. This is why we &#39:ve launched Quimby. One click and you can generate a beautifully designed website in seconds.</div>
            </div>
          </div>
        </div>
<Footer />
      </main>
    </>
  );
};
export default Home