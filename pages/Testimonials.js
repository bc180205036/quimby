import React from 'react'
import Header from '@/component/header';
import Footer from '@/component/Footer';
import Image from 'next/image';
function Testimonials() {

  return (
    <div><Header/>
    <div className='container  mt-5 pt-5 mb-5 '>
      <div className='row'>
        <div className='col-5'>
        
            <div className='row mini-card pt-2'>
              <div className='col-4 pt-2 pb-2'><Image className='picture1'src="/4.jpg" width={150} height={150} alt='my'/></div>

              <div className='col'><div><span className='mini-card-heading'>Jerry Quan</span></div>
              <div><p className='mini-card-text2'>Jerry&#39;s Ice Cream Shop</p></div>
              <div className='row mini-card-text'>I Am in love with my website. Quimby has made it so easy to have a website that looks amazing yet so easy to manage.</div></div>
              
            </div>
            <div className='row mini-card pt-2 mt-4'>
              <div className='col-4 pt-2 pb-2'><Image className='picture1'src="/4.jpg" width={150} height={150} alt='my'/></div>

              <div className='col'><div><span className='mini-card-heading'>Jerry Quan</span></div>
              <div><p className='mini-card-text2'>Jerry&#39;s Ice Cream Shop</p></div>
              <div className='row mini-card-text'>I Am in love with my website. Quimby has made it so easy to have a website that looks amazing yet so easy to manage.</div></div>
              
            </div>
            <div className='row mini-card pt-2 mt-4'>
              <div className='col-4 pt-2 pb-2'><Image className='picture1'src="/4.jpg" width={150} height={150} alt='my'/></div>

              <div className='col'><div><span className='mini-card-heading'>Jerry Quan</span></div>
              <div><p className='mini-card-text2'>Jerry&#39;s Ice Cream Shop</p></div>
              <div className='row mini-card-text'>I Am in love with my website. Quimby has made it so easy to have a website that looks amazing yet so easy to manage.</div></div>
              
            </div>
           
           
        </div>
        <div className='col vh-auto ms-5 video-card'> <iframe  className="video" src="https://www.youtube.com/embed/NqjKpE7bdXc?si=6GK6PtYjf73xCsi0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> </div>

      </div>
    </div>
    <div className='container mb-5'>
      <div className='row'>
        <div className='col me-5 '>
        <div className='row mini-card pt-2 mt-4'>
              <div className='col-4 pt-2 pb-2'><Image className='picture1'src="/4.jpg" width={150} height={150} alt='my'/></div>

              <div className='col pt-5'><div><span className='mini-card-heading'>Jerry Quan</span></div>
              <div><p className='mini-card-text2'>Jerry&#39;s Ice Cream Shop</p></div>
             </div>
             <div className='row mini-card-text ps-4'>I Am in love with my website. Quimby I Am in love with my website. Quimby has made it so easy to have a website Am in love with my website. Quimby has made it so easy to have a website that looks amazing yet so easy to manage. that looks amazing yet so easy to manage. has made it so easy to have a website that looks amazing yet so easy to manage. I Am in love with my website. Quimby I Am in love with my website. Quimby has made it so easy to have a website Am in love with my website. Quimby has made it so easy to have a website that looks amazing yet so easy to manage. that looks amazing yet so easy to manage. has made it so easy to have a website that looks amazing yet so easy to manage.
             </div>
              
            </div>

        </div>
        <div className='col me-5'>
        <div className='row mini-card pt-2 mt-4'>
              <div className='col-4 pt-2 pb-2'><Image className='picture1'src="/4.jpg" width={150} height={150} alt='my'/></div>

              <div className='col pt-5'><div><span className='mini-card-heading'>Jerry Quan</span></div>
              <div><p className='mini-card-text2'>Jerry&#39;s Ice Cream Shop</p></div>
             </div>
             <div className='row ps-4 mini-card-text'>I Am in love with my website. Quimby I Am in love with my website. Quimby has made it so easy to have a website Am in love with my website. Quimby has made it so easy to have a website that looks amazing yet so easy to manage. that looks amazing yet so easy to manage. has made it so easy to have a website that looks amazing yet so easy to manage. I Am in love with my website. Quimby I Am in love with my website. Quimby has made it so easy to have a website Am in love with my website. Quimby has made it so easy to have a website that looks amazing yet so easy to manage. that looks amazing yet so easy to manage. has made it so easy to have a website that looks amazing yet so easy to manage.
             </div>
              
            </div>

        </div>
      </div>
    </div>
    <Footer/>
    </div>
  )
}

export default Testimonials