import React from 'react'
import Header from '@/component/header'
import Footer from '@/component/Footer'
import Inputfield from '@/component/inputfield'
function GoCustom() {
  return (
    <div><Header/>
    <div className='container main-card mt-5 '>
          <div className='row d-flex justify-content-center'>
            <div className='col-7 mt-5'>
              <div className=' me-5 d-flex  justify-content-center mt-5 mb-4'><div className='col-6'><span className='heading0 ps-5'>Go Custom</span></div></div>
              
              
              
              
              <div className='ms-4 mb-4 '>&nbsp;&nbsp;Some projects are much bigger than others and some dreams need a lot more to make them<br/> work. If you desire a custom website, add your email and we will reach out with more information.</div>
              <div className='d-flex justify-content-center mb-5'><div className='col-8'><Inputfield/></div></div>
            </div>
          </div>
        </div>
    <Footer/>
    </div>
  )
}

export default GoCustom