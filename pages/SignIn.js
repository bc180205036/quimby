 import React from 'react'
import Header from '@/component/header'
import Footer from '@/component/Footer'
function SignIn() {
  return (
    <div><Header />
    <div className='container mt-5 pt-5 mb-5 pb-5'>
      <div className='row  d-flex justify-content-center mb-5 pb-5'>
        <div className='col-6 signin-card1'>
          <div className='d-flex justify-content-center' ><span className='heading1-sign-in'>Sign In</span></div>
          <form className='ms-3 me-3'>
            <div><label className='label'>Email or Phone Number</label></div>
            <div><input type='text' className=' signin-input mb-3 ps-3' /></div>
            <div><label className='label '>Password</label></div>
            <div><input type='text' className=' signin-input ps-3' /></div>
            <div className='d-flex justify-content-center mt-5 mb-5 '><button className='confirm-button'>Confirm</button></div>
          </form>
        </div>
      </div>
    </div>
    <Footer/>
    </div>
  )
}

export default SignIn